package minTic.mibarrio.service;

import java.util.List;
import javax.transaction.Transactional;
import minTic.mibarrio.model.Cliente;
import minTic.mibarrio.repository.IClienteRepositoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ClienteService implements IClienteService {

    @Autowired
    private IClienteRepositoryDao _IClienteRepositoryDao;

    @Override
    public List<Cliente> getClientes() 
    {
        return _IClienteRepositoryDao.findAll();
    }

    @Override
    public Cliente getCliente(Integer id) 
    {
        return _IClienteRepositoryDao.findById(id).orElse(null);
    }

    @Override
    public Cliente grabarCliente(Cliente cliente) 
    {
        return _IClienteRepositoryDao.save(cliente);
    }

    @Override
    public void delete(Integer id) 
    {
        _IClienteRepositoryDao.deleteById(id);
    }
}
