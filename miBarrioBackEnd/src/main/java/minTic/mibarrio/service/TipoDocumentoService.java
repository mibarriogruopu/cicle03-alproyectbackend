package minTic.mibarrio.service;

import java.util.List;
import javax.transaction.Transactional;
import minTic.mibarrio.model.TipoDocumento;
import minTic.mibarrio.repository.ITipoDocumentoRepositoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class TipoDocumentoService implements ITipoDocumentoService {

    @Autowired
    private ITipoDocumentoRepositoryDao _ITipoDocumentoRepositoryDao;
    
    @Override
    public List<TipoDocumento> getTipoDocumentos()
    {
        return _ITipoDocumentoRepositoryDao.findAll();
    }
    
    @Override
    public TipoDocumento getTipoDocumento(Integer id) 
    {
        return _ITipoDocumentoRepositoryDao.findById(id).orElse(null);
    }
    
    @Override
    public TipoDocumento grabarTipoDocumento(TipoDocumento typeDocument) 
    {
        return _ITipoDocumentoRepositoryDao.save(typeDocument);
    }
    
    @Override
    public void delete(Integer id) 
    {
        _ITipoDocumentoRepositoryDao.deleteById(id);
    }
}
