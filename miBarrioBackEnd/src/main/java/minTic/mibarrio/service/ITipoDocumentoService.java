package minTic.mibarrio.service;

import java.util.List;
import minTic.mibarrio.model.TipoDocumento;

public interface ITipoDocumentoService {
    public List<TipoDocumento> getTipoDocumentos   ();
    public TipoDocumento       getTipoDocumento    (Integer id);
    public TipoDocumento       grabarTipoDocumento (TipoDocumento typeDocument);
    public void                delete              (Integer id);
}
