package minTic.mibarrio.service;

import minTic.mibarrio.model.Cliente;
import java.util.List;

public interface IClienteService {
    
    public List<Cliente> getClientes();
    public Cliente       getCliente(Integer id);
    public Cliente       grabarCliente(Cliente cliente);
    public void          delete(Integer id);
}
