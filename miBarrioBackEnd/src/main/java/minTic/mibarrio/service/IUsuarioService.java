package minTic.mibarrio.service;

import java.util.List;
import minTic.mibarrio.model.Usuario;

public interface IUsuarioService {
    public List<Usuario> getUsuarios  ();
    public Usuario       getUsuario   (Integer id);
    public Usuario       grabarUsuario(Usuario user);
    public void          delete       (Integer id);
}
