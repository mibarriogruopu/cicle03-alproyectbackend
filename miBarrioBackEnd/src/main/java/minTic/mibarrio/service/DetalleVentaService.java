package minTic.mibarrio.service;

import java.util.List;
import javax.transaction.Transactional;
import minTic.mibarrio.model.DetalleVenta;
import minTic.mibarrio.repository.IDetalleVentaRepositoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class DetalleVentaService implements IDetalleVentaService 
{

    @Autowired
    private IDetalleVentaRepositoryDao _IDetalleVentaRepositoryDao;

    @Override
    public List<DetalleVenta> getDetalleVentas()
    {
        return _IDetalleVentaRepositoryDao.findAll();
    }
    
    @Override
    public DetalleVenta getVenta(Integer id)
    {
        return _IDetalleVentaRepositoryDao.findById(id).orElse(null);
    }
    
    @Override
    public DetalleVenta grabarDetalleVenta(DetalleVenta detalleVenta)
    {
        return _IDetalleVentaRepositoryDao.save(detalleVenta);
    }
    
    @Override
    public void delete(Integer id)
    {
       _IDetalleVentaRepositoryDao.deleteById(id);
    }    
}
