
package minTic.mibarrio.service;

import java.util.List;
import javax.transaction.Transactional;
import minTic.mibarrio.model.Venta;
import minTic.mibarrio.repository.IVentaRepositoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class VentaService implements IVentaService{
    @Autowired
    private IVentaRepositoryDao _IVentaRepositoryDao;
    
    @Override
    public List<Venta> getVentas()
    {
        return _IVentaRepositoryDao.findAll();
    }
    
    @Override
    public Venta getVenta(Integer id)
    {
        return _IVentaRepositoryDao.findById(id).orElse(null);
    }
    
    @Override
    public Venta grabarVenta(Venta venta)
    {
        return _IVentaRepositoryDao.save(venta);
    }
    
    @Override
    public void delete(Integer id)
    {
      _IVentaRepositoryDao.deleteById(id);
    }
}
