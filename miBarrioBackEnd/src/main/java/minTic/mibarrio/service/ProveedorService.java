package minTic.mibarrio.service;

import java.util.List;
import javax.transaction.Transactional;
import minTic.mibarrio.model.Proveedor;
import minTic.mibarrio.repository.IProveedorRespositoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProveedorService implements IProveedorService 
{

    @Autowired
    private IProveedorRespositoryDao _IProveedorRespositoryDao;

    @Override
    public List<Proveedor> getProveedores() 
    {
        return _IProveedorRespositoryDao.findAll();
    }
    
    @Override
    public Proveedor getProveedor(Integer Id) 
    {
        return _IProveedorRespositoryDao.findById(Id).orElse(null);
    }
    
    @Override
    public Proveedor grabarProveedor(Proveedor proveed) 
    {
        return _IProveedorRespositoryDao.save(proveed);
    }
    
    @Override
    public void delete(Integer id) 
    {
        _IProveedorRespositoryDao.deleteById(id);
    }
}
