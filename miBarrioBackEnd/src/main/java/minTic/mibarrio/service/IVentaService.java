package minTic.mibarrio.service;

import java.util.List;
import minTic.mibarrio.model.Venta;


public interface IVentaService {
    public List<Venta> getVentas  ();
    public Venta       getVenta   (Integer id);
    public Venta       grabarVenta(Venta venta);
    public void        delete     (Integer id);
}
