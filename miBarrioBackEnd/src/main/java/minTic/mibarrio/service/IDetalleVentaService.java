package minTic.mibarrio.service;

import java.util.List;
import minTic.mibarrio.model.DetalleVenta;
public interface IDetalleVentaService {
    public List<DetalleVenta> getDetalleVentas  ();
    public DetalleVenta       getVenta          (Integer id);
    public DetalleVenta       grabarDetalleVenta(DetalleVenta detalleVenta);
    public void               delete            (Integer id);
}
