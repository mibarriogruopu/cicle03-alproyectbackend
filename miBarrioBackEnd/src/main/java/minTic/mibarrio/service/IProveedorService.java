package minTic.mibarrio.service;

import java.util.List;
import minTic.mibarrio.model.Proveedor;

public interface IProveedorService {
    public List<Proveedor> getProveedores ();
    public Proveedor       getProveedor   (Integer Id);
    public Proveedor       grabarProveedor(Proveedor proveed);
    public void            delete         (Integer id);
}
