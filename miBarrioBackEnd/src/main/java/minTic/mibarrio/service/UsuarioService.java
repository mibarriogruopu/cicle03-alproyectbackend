
package minTic.mibarrio.service;

import java.util.List;
import javax.transaction.Transactional;
import minTic.mibarrio.model.Usuario;
import minTic.mibarrio.repository.IUsuarioRepositoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UsuarioService implements IUsuarioService{
    @Autowired
    private IUsuarioRepositoryDao _IUsuarioRepositoryDao;
    
 @Override
 public List<Usuario> getUsuarios()
 {
     return _IUsuarioRepositoryDao.findAll();
 }
 
 @Override
 public Usuario getUsuario(Integer id)
 {
     return _IUsuarioRepositoryDao.findById(id).orElse(null);
 }
 
 @Override
 public Usuario grabarUsuario(Usuario user)
 {
     return _IUsuarioRepositoryDao.save(user);
 }
 
 @Override
 public void delete(Integer id)
 {
     _IUsuarioRepositoryDao.deleteById(id);
 }
}
