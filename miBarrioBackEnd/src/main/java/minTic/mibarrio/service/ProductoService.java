package minTic.mibarrio.service;

import java.util.List;
import javax.transaction.Transactional;
import minTic.mibarrio.model.Producto;
import minTic.mibarrio.repository.IProductoRepositoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductoService implements IProductoService 
{
    @Autowired
    private IProductoRepositoryDao _IProductoRepositoryDao;
    
    @Override
    public List<Producto> getProductos() 
    {
        return _IProductoRepositoryDao.findAll();
    }
    
    @Override
    public Producto getProducto(Integer id) 
    {
        return _IProductoRepositoryDao.findById(id).orElse(null);
    }
    
    @Override
    public Producto grabarProducto(Producto product) 
    {
        return _IProductoRepositoryDao.save(product);
    }
    
    @Override
    public void delete(Integer id) 
    {
        _IProductoRepositoryDao.deleteById(id);
    }
}
