package minTic.mibarrio.service;

import java.util.List;
import minTic.mibarrio.model.Producto;

public interface IProductoService {
    public List<Producto> getProductos  ();
    public Producto       getProducto   (Integer id);
    public Producto       grabarProducto(Producto product);
    public void           delete        (Integer id);
}
