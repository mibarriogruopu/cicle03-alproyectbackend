package minTic.mibarrio.controller;

import java.util.List;
import minTic.mibarrio.model.Proveedor;
import minTic.mibarrio.repository.IProveedorRespositoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/Proveedor")
public class ProveedorController {
    @Autowired
    private IProveedorRespositoryDao _IProveedorRespositoryDao;
    
    @RequestMapping(value = "/list", method = RequestMethod.GET )
    public List<Proveedor> listarProveedores(){
        return (_IProveedorRespositoryDao.findAll());
    }
}
