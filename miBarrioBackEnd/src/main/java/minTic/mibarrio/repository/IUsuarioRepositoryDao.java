package minTic.mibarrio.repository;

import minTic.mibarrio.model.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface IUsuarioRepositoryDao extends CrudRepository<Usuario, Integer>{
    
}
