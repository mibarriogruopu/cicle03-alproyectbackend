
package minTic.mibarrio.repository;

import minTic.mibarrio.model.DetalleVenta;
import org.springframework.data.repository.CrudRepository;

public interface IDetalleVentaRepositoryDao extends CrudRepository<DetalleVenta, Integer>{
    
}
