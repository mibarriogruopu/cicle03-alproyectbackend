
package minTic.mibarrio.repository;

import minTic.mibarrio.model.Proveedor;
import org.springframework.data.repository.CrudRepository;


public interface IProveedorRespositoryDao extends CrudRepository<Proveedor, Integer>{
    
}
