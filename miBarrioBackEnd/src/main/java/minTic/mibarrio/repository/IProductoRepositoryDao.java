
package minTic.mibarrio.repository;

import minTic.mibarrio.model.Producto;
import org.springframework.data.repository.CrudRepository;

public interface IProductoRepositoryDao extends CrudRepository<Producto, Integer>{
    
}
