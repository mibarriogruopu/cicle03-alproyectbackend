
package minTic.mibarrio.repository;

import minTic.mibarrio.model.TipoDocumento;
import org.springframework.data.repository.CrudRepository;


public interface ITipoDocumentoRepositoryDao extends CrudRepository<TipoDocumento, Integer>{
    
}
