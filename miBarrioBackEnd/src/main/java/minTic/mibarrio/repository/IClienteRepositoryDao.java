
package minTic.mibarrio.repository;

import minTic.mibarrio.model.Cliente;
import org.springframework.data.repository.CrudRepository;


public interface IClienteRepositoryDao extends CrudRepository<Cliente, Integer>{
    
}
