package minTic.mibarrio.repository;

import minTic.mibarrio.model.Venta;
import org.springframework.data.repository.CrudRepository;

public interface IVentaRepositoryDao extends CrudRepository<Venta, Integer>{
    
}
