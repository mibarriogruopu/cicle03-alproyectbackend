
package minTic.mibarrio.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table (name = "venta")
public class Venta implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    @Column (name="id")
    private int    id;
    
    @Column (name="idCliente")
    @JoinColumn(name = "id")
    private Cliente idCliente;
    
    @Column (name="idUsuario")
    @JoinColumn(name = "id")
    private Usuario idUsuario;
    
    @Column (name="ivaVenta")
    private double ivaVenta;
    
    @Column (name="totalVenta")
    private double totalVenta;
    
    @Column (name="valorVenta")
    private double valorVenta;

    public Venta() {
    }

    public Venta(int id, int idCliente, int idUsuario, double ivaVenta, double totalVenta, double valorVenta) {
        this.id = id;
        this.idCliente = idCliente;
        this.idUsuario = idUsuario;
        this.ivaVenta = ivaVenta;
        this.totalVenta = totalVenta;
        this.valorVenta = valorVenta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public double getIvaVenta() {
        return ivaVenta;
    }

    public void setIvaVenta(double ivaVenta) {
        this.ivaVenta = ivaVenta;
    }

    public double getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(double totalVenta) {
        this.totalVenta = totalVenta;
    }

    public double getValorVenta() {
        return valorVenta;
    }

    public void setValorVenta(double valorVenta) {
        this.valorVenta = valorVenta;
    }

    @Override
    public String toString() {
        return "Venta{" + "id=" + id + ", idCliente=" + idCliente + ", idUsuario=" + idUsuario + ", ivaVenta=" + ivaVenta + ", totalVenta=" + totalVenta + ", valorVenta=" + valorVenta + '}';
    }
    
    
}
