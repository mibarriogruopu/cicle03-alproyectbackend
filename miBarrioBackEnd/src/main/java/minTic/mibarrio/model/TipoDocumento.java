package minTic.mibarrio.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipodocumento")
public class TipoDocumento implements Serializable 
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    @Column (name = "id")
    private int id;
    
    @Column (name = "tipo")
    private String tipo;

    public TipoDocumento() {
    }

    public TipoDocumento(int id, String tipo) {
        this.id = id;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "TipoDocumento{" + "id=" + id + ", tipo=" + tipo + '}';
    }
     
}
