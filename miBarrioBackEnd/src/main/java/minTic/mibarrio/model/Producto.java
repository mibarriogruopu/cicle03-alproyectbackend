package minTic.mibarrio.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class Producto implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column (name="id")
    private int id;
    
    @Column (name="id")
    @JoinColumn(name = "id")
    private Proveedor idProveedor;
    
    @Column (name="id")
    private double ivaCompra;
    
    @Column (name="id")
    private String nombre;
    
    @Column (name="id")
    private double precioCompra;
    
    @Column (name="id")
    private double precioVenta;

    public Producto() {
    }

    public Producto(int id, int idProveedor, double ivaCompra, String nombre, double precioCompra, double precioVenta) {
        this.id = id;
        this.idProveedor = idProveedor;
        this.ivaCompra = ivaCompra;
        this.nombre = nombre;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public double getIvaCompra() {
        return ivaCompra;
    }

    public void setIvaCompra(double ivaCompra) {
        this.ivaCompra = ivaCompra;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(double precioCompra) {
        this.precioCompra = precioCompra;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", idProveedor=" + idProveedor + ", ivaCompra=" + ivaCompra + ", nombre=" + nombre + ", precioCompra=" + precioCompra + ", precioVenta=" + precioVenta + '}';
    }
}
