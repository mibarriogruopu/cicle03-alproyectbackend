
package minTic.mibarrio.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table (name = "detalleventa")
public class DetalleVenta implements Serializable{
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column (name="id")
   private int    id;
   
   @Column (name="idProducto")
   @JoinColumn(name = "id")
   private Producto idProducto;
   
   @Column (name="idVenta")
   @JoinColumn(name = "id")
   private Venta  idVenta;
   
   @Column (name="cantidadProducto")
   private int    cantidadProducto;
   
   @Column (name="valorTotal")
   private double valorTotal;
   
   @Column (name="valorVenta")
   private double valorVenta;
   
   @Column (name="valorIva")
   private double valorIva;

    public DetalleVenta() {
    }

    public DetalleVenta(int id, int idProducto, int idVenta, int cantidadProducto, double valorTotal, double valorVenta, double valorIva) {
        this.id = id;
        this.idProducto = idProducto;
        this.idVenta = idVenta;
        this.cantidadProducto = cantidadProducto;
        this.valorTotal = valorTotal;
        this.valorVenta = valorVenta;
        this.valorIva = valorIva;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    public int getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(int cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public double getValorVenta() {
        return valorVenta;
    }

    public void setValorVenta(double valorVenta) {
        this.valorVenta = valorVenta;
    }

    public double getValorIva() {
        return valorIva;
    }

    public void setValorIva(double valorIva) {
        this.valorIva = valorIva;
    }
   
   
}
