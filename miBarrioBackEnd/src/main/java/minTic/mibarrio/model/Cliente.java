
package minTic.mibarrio.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table (name = "cliente")
public class Cliente implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    @Column (name = "id")
    private int id;
    
    @Column (name = "idTipoDocumento")
    @JoinColumn(name = "id")
    private TipoDocumento idTipoDocumento;

    @Column (name = "numeroDocumento")    
    private String numeroDocumento;

    @Column (name = "direccion")    
    private String direccion;
    
    @Column (name = "email")
    private String email;
    
    @Column (name = "nombre")
    private String nombre;
    
    @Column (name = "telefono")
    private String telefono;

    public Cliente() {
    }

    public Cliente(int id, int idTipoDocumento, String numeroDocumento, String direccion, String email, String nombre, String telefono) {
        this.id = id;
        this.idTipoDocumento = idTipoDocumento;
        this.numeroDocumento = numeroDocumento;
        this.direccion = direccion;
        this.email = email;
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Cliente{" + "id=" + id + ", idTipoDocumento=" + idTipoDocumento + ", numeroDocumento=" + numeroDocumento + ", direccion=" + direccion + ", email=" + email + ", nombre=" + nombre + ", telefono=" + telefono + '}';
    }
    
    
}
