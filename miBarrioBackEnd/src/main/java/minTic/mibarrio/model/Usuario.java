
package minTic.mibarrio.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "usuario")
public class Usuario implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    @Column (name="id")
    private int id;
    
    @Column (name="idTipoDocumento")
    private int idTipoDocumento;
    
    @Column (name="numeroDocumento")
    private String numeroDocumento;
    
    @Column (name="email")
    private String email;
    
    @Column (name="nombre")
    private String nombre;
    
    @Column (name="password")
    private String password;
    
    @Column (name="nombreUsuario")
    private String nombreUsuario;
    
     public Usuario() {
    }

    public Usuario(int id, int idTipoDocumento, String numeroDocumento, String email, String nombre, String password, String nombreUsuario) {
        this.id = id;
        this.idTipoDocumento = idTipoDocumento;
        this.numeroDocumento = numeroDocumento;
        this.email = email;
        this.nombre = nombre;
        this.password = password;
        this.nombreUsuario = nombreUsuario;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", idTipoDocumento=" + idTipoDocumento + ", numeroDocumento=" + numeroDocumento + ", email=" + email + ", nombre=" + nombre + ", password=" + password + ", nombreUsuario=" + nombreUsuario + '}';
    }

}